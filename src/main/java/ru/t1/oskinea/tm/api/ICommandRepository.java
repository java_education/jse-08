package ru.t1.oskinea.tm.api;

import ru.t1.oskinea.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
